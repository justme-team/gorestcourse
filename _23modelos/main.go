package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	router := mux.NewRouter().StrictSlash(true)

	router.HandleFunc("/", greet)
	router.HandleFunc("/movies", movieList)
	router.HandleFunc("/movies/{id}", movieID)

	server := http.ListenAndServe(":2314", router)
	log.Fatal(server)
}

func greet(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hola mundo")
}

func movieList(w http.ResponseWriter, r *http.Request) {
	movies := Movies{
		Movie{"Pelicula 1", 2014, "Unnamed"},
		Movie{"Pelicula 2", 2015, "Unnamed"},
		Movie{"Pelicula 3", 2016, "Unnamed"},
	}

	json.NewEncoder(w).Encode(movies)
}

func movieID(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	movieID := params["id"]
	fmt.Fprintf(w, "Movie with id %s", movieID)
}
