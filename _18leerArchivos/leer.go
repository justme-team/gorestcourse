package main

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
)

func main() {
	fmt.Println("Lector")
	absPath, _ := filepath.Abs("./_18leerArchivos/fichero.txt")
	fichero, error := ioutil.ReadFile(absPath)
	showError(error)
	fmt.Println(string(fichero))
}

func showError(e error) {
	if e != nil {
		panic(e) //Die php
	}
}
