package main

import (
	"fmt"
)

func main() {
	numero1 := 10
	numero2 := 6
	suma := numero1 + numero2
	resta := numero1 - numero2
	multiplicacion := numero1 * numero2
	division := numero1 / numero2
	modulo := numero1 % numero2

	fmt.Printf("Suma: %v \n", suma)
	fmt.Printf("Resta: %v \n", resta)
	fmt.Printf("Multiplicacion: %v \n", multiplicacion)
	fmt.Printf("Division: %v \n", division)
	fmt.Printf("Modulo: %v \n", modulo)
}
