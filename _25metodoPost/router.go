package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type Route struct {
	Method      string
	Path        string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var routes = Routes{
	Route{"GET", "/", Index},
	Route{"GET", "/movies", MovieList},
	Route{"GET", "/movie/{id}", MovieID},
	Route{"POST", "/movie", MovieAdd},
}

func NewRouter() *mux.Router {
	log.Println("Listening on port 2314")
	router := mux.NewRouter().StrictSlash(true)

	for _, route := range routes {
		router.Methods(route.Method).
			Path(route.Path).
			Handler(route.HandlerFunc)
	}
	return router
}
