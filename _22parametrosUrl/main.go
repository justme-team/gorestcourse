package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/movies", movieList)
	router.HandleFunc("/movies/{id}", movieShow)

	server := http.ListenAndServe(":2314", router)
	log.Fatal(server)
}

func movieList(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Movies List")
}

func movieShow(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	movieID := params["id"]
	fmt.Fprintf(w, "Loaded Movie With id %s", movieID)
}
