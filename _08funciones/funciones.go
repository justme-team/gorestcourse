package main

import (
	"fmt"
)

func main() {
	var numero1 float32 = 5
	var numero2 float32 = 2

	fmt.Printf("Suma: %v\n", operacion(numero1, numero2, "+"))
	fmt.Printf("Resta: %v\n", operacion(numero1, numero2, "-"))
	fmt.Printf("Multiplicacion: %v\n", operacion(numero1, numero2, "*"))
	fmt.Printf("Division: %v\n", operacion(numero1, numero2, "/"))
}

func operacion(n1 float32, n2 float32, operacion string) float32 {
	switch operacion {
	case "+":
		return n1 + n2
		break
	case "-":
		return n1 - n2
		break
	case "*":
		return n1 * n2
		break
	case "/":
		return n1 / n2
		break
	}
	return 0
}
