//Slice array que no tiene límite
package main

import (
	"fmt"
)

func main() {
	peliculas := []string{"blah1", "blah2", "blah3", "blah4"}
	peliculas = append(peliculas, "infiltrado:D")
	fmt.Println(peliculas)
	//Funcion Len
	fmt.Println(len(peliculas))
	//Caracteristicas rango
	fmt.Println(peliculas[1:3])
}
