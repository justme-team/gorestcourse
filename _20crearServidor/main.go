package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/", func(res http.ResponseWriter, req *http.Request) {
		fmt.Println("Hola mundo desde mi servidor web con go")
	})

	server := http.ListenAndServe(":2413", nil)
	log.Fatal(server)
}
