package main

import (
	"fmt"
)

func main() {
	var peliculas [3]string
	peliculas[0] = "Blah1"
	peliculas[1] = "Blah2"
	peliculas[2] = "Blah3"

	fmt.Println(peliculas[1])

	peliculas2 := [3]string{
		"Blah Recargado 1",
		"Blah Recargado 2",
		"Blah Recargado 3"}
	fmt.Println(peliculas2)
}
