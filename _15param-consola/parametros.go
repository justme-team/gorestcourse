package main

import (
	"fmt"
	"os"
	"strconv"
)

func main() {
	fmt.Println(os.Args[1])

	edad, err := strconv.Atoi(os.Args[2])
	// edad, _ := strconv.Atoi(os.Args[2]) //También funciona
	if err != nil {
		fmt.Println("Error")
	}
	fmt.Printf("%T: %v\n", edad, edad)
}
