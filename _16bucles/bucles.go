package main

import (
	"fmt"
)

func main() {
	peliculas := []string{"pel1", "pel2", "pel3", "pelusa"}

	for i := 0; i < len(peliculas); i++ {
		fmt.Println("La pelicula", peliculas[i], "tiene indice", validarCaracteristica(i))
	}
	//Otra forma de hacer for (forEach)
	for _, pelicula := range peliculas {
		fmt.Println("La pelicula", pelicula, " no tiene indice asigando e var")
	}
}
func validarCaracteristica(numero int) string {
	if numero%2 == 0 {
		return "par"
	} else {
		return "impar"
	}
}
