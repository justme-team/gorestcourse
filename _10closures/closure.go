//Closure, función dentro de otra función
package main

import (
	"fmt"
)

func main() {
	fmt.Println(gorras(24, "$"))
	pantalon("Rojo", "corto", "Jean")
}

func pantalon(caracteristicas ...string) {
	for i, caracteristica := range caracteristicas {
		fmt.Printf("%v. %v\n", i, caracteristica)
	}
}

func gorras(pedido float32, moneda string) (string, string, float32) {
	precio := func() float32 {
		return pedido * 7
	}
	return "El precio del pedido es", moneda, precio()
}
