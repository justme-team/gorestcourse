package main

import (
	"fmt"
)

func main() {
	var peliculas [3][2]string

	peliculas[0][0] = "Blah00"
	peliculas[0][1] = "Blah01"
	peliculas[1][0] = "Blah10"
	peliculas[1][1] = "Blah11"
	peliculas[2][0] = "Blah20"
	peliculas[2][1] = "Blah21"

	fmt.Println(peliculas[1])
}
