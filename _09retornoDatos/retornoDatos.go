package main

import (
	"fmt"
)

func main() {
	fmt.Println(devolverTexto())
	fmt.Println(devolverTexto2())
}
func devolverTexto() (string, string) {
	return "Hola", "Andrés"
}
func devolverTexto2() (dato1 string, dato2 int) { //Mejores practicas
	dato1 = "David"
	dato2 = 26
	return
}
