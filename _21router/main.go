package main

import "github.com/gorilla/mux"
import "net/http"
import "fmt"
import "log"

func main() {
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/", index)
	router.HandleFunc("/contacto", conctact)

	server := http.ListenAndServe(":2314", router)
	log.Fatal(server)

}

func index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hola mundo desde go")
}

func conctact(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Esta es la página de contacto")
}
