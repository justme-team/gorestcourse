package main

import (
	"fmt"
	"os"
	"path/filepath"
)

func main() {
	fmt.Println("Lector:")

	inputText := string(os.Args[1] + "\n")
	absPath, _ := filepath.Abs("./_19escribirArchivos/fichero.txt")
	fmt.Println(string(inputText))

	// escribir := ioutil.WriteFile(absPath, inputText, 0777)
	fichero, err := os.OpenFile(absPath, os.O_APPEND|os.O_WRONLY, 0777)
	showError(err)

	escribir, err := fichero.WriteString(inputText)
	showError(err)
	fmt.Println(escribir)

	fichero.Close()
}

func showError(e error) {
	if e != nil {
		panic(e)
	}
}
