package main

import (
	"fmt"
)

type Gorra struct {
	marca  string
	color  string
	precio float32
	plana  bool
}

func main() {
	// var gorraNegra = Gorra{
	// 	marca:  "nike",
	// 	color:  "negro",
	// 	precio: 25,
	// 	plana:  false}

	var gorraNegra = Gorra{"nike", "negro", 25, false}

	fmt.Println(gorraNegra)
	fmt.Println(gorraNegra.marca)
}
