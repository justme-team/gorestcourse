package main

import (
	"fmt"
	"time"
)

func main() {
	momento := time.Now()
	hoy := momento.Weekday()

	switch hoy {
	case 0:
		fmt.Println("Hoy es domingo")
	case 1:
		fmt.Println("Hoy es lunes")
	case 2:
		fmt.Println("Hoy es martes")
	default:
		fmt.Println("Hoy puede ser cualquier otro dia")
	}
}
