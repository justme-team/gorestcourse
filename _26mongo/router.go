package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type Route struct {
	Method      string
	Path        string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var routes = Routes{
	Route{"GET", "/", Index},
	Route{"GET", "/movies", MoviesList},

	Route{"POST", "/movie", MovieAdd},
	Route{"GET", "/movie/{id}", MovieShow},
	Route{"PUT", "/movie/{id}", MovieUpdate},
	Route{"DELETE", "/movie/{id}", MovieDelete},
}

func NewRouter() *mux.Router {
	log.Println("Listening on http://localhost:2314")
	router := mux.NewRouter().StrictSlash(true)

	for _, route := range routes {
		router.Methods(route.Method).
			Path(route.Path).
			HandlerFunc(route.HandlerFunc)
	}
	return router
}
