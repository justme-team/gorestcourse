package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"

	"gopkg.in/mgo.v2"
)

var collection = getSession().DB("curso_mongo").C("movies")

func getSession() *mgo.Session {
	session, err := mgo.Dial("mongodb://localhost")

	if err != nil {
		panic(err)
	}

	return session
}

func responseMovie(w http.ResponseWriter, status int, result Movie) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(result)
}

func responseMovies(w http.ResponseWriter, status int, results Movies) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(results)
}
func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hola mundo")
}

func MoviesList(w http.ResponseWriter, r *http.Request) {
	var results Movies
	err := collection.Find(nil).Sort("-_id").All(&results)
	if err != nil {
		log.Fatal(err)
	}

	responseMovies(w, 200, results)
}

func MovieAdd(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var movieData Movie
	err := decoder.Decode(&movieData)

	if err != nil {
		panic(err)
	}

	defer r.Body.Close()

	cErr := collection.Insert(movieData)

	if cErr != nil {
		w.WriteHeader(500)
		return
	}

	responseMovie(w, 200, movieData)
}
func MovieShow(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	movieID := params["id"]

	if !bson.IsObjectIdHex(movieID) {
		w.WriteHeader(404)
		return
	}

	objectID := bson.ObjectIdHex(movieID)
	results := Movie{}
	err := collection.FindId(objectID).One(&results)

	if err != nil {
		w.WriteHeader(404)
		return
	}

	responseMovie(w, 200, results)
}
func MovieUpdate(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	movieID := params["id"]

	if !bson.IsObjectIdHex(movieID) {
		w.WriteHeader(404)
		return
	}

	objectID := bson.ObjectIdHex(movieID)
	decoder := json.NewDecoder(r.Body)

	var movieData Movie
	err := decoder.Decode(&movieData)

	if err != nil {
		panic(err)
		w.WriteHeader(500)
		return
	}

	defer r.Body.Close()

	document := bson.M{"_id": objectID}
	change := bson.M{"$set": movieData}

	err = collection.Update(document, change)

	if err != nil {
		panic(err)
		w.WriteHeader(404)
		return
	}

	responseMovie(w, 200, movieData)
}

type Message struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

func MovieDelete(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	movieID := params["id"]

	if !bson.IsObjectIdHex(movieID) {
		w.WriteHeader(404)
		return
	}

	objectID := bson.ObjectIdHex(movieID)
	err := collection.RemoveId(objectID)

	if err != nil {
		w.WriteHeader(404)
		return
	}

	result := Message{"Succes", "La película con ID " + movieID + " ha sido eliminada correctamente"}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(result)
}
